package com.example.StreamAPI.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
@Service
public class PhoneBookCrawler {

    private PhoneBook phoneBook;

    public PhoneBookCrawler(PhoneBook phoneBook) {
        this.phoneBook = phoneBook;
    }

    public String findPhoneNumberByNameAndPunishIfNothingFound(String name){
        return phoneBook.getPhoneBookEntries()
                .entrySet()
                .stream()
                .filter(e -> e.getKey().equals(name))
                .map(Map.Entry::getValue)
                .map(Objects::toString)
                .findAny().orElseThrow(() -> new IllegalArgumentException("No phone number found"));

    }

    public String findPhoneNumberByNameAndPrintPhoneBookIfNothingFound(String name){

        return phoneBook.getPhoneBookEntries()
                .entrySet()
                .stream()
                .filter(e -> e.getKey().equals(name))
                .map(Map.Entry::getValue)
                .map(Objects::toString)
                .findAny().orElseGet(()->{return String.valueOf(phoneBook);});
    }

    public String findPhoneNumberByNameOrNameByPhoneNumber(String name, String phoneNumber){
        return phoneBook.getPhoneBookEntries()
                .entrySet()
                .stream()
                .filter(e -> e.getKey().equals(name))
                .map(Map.Entry::getValue)
                .map(Objects::toString)
                .findAny().orElseGet( () -> {
                    return phoneBook.getPhoneBookEntries()
                            .entrySet()
                            .stream()
                            .filter(e -> e.getValue().equals(phoneNumber))
                            .map(Map.Entry::getKey)
                            .map(Objects::toString)
                            .findAny().orElse(String.valueOf(phoneBook.getPhoneBookEntries().entrySet().stream()
                                    .filter(a -> a.getKey().equals("Jos de Vos"))
                                    .map(Map.Entry::getValue)
                                    .map(Object::toString)
                                    .findAny()));
                });
    }

    public PhoneBook getPhoneBook(){
        return phoneBook;
    }

}
