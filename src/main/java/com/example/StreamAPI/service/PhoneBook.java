package com.example.StreamAPI.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
/*
Implement findPhoneNumberByName and findNameByPhoneNumber in PhoneBook class that returns an optional.
An empty optional must be returned if nothing is found.
 */

@AllArgsConstructor
@NoArgsConstructor
@Service
public class PhoneBook {

    private static final HashMap<String, String> PHONE_NUMBERS = new HashMap<String, String>() {
        {
            put("Jos de Vos", "016/161616");
            put("An de Toekan", "016/161617");
            put("Kris de Vis", "016/161618");
        }
    };

    private HashMap<String, String> phoneBookEntries = PHONE_NUMBERS;


    public HashMap<String, String> getPhoneBookEntries() {
        return phoneBookEntries;
    }

    public Optional<String> findPhoneNumberByName(String name){
        return phoneBookEntries.entrySet().stream()
                .filter(a -> a.getKey().equals(name))
                .map(Map.Entry::getValue)
                .map(Object::toString)
                .findAny();
    }

    public Optional<String> findNameByPhoneNumber(String phoneNumber){
        return phoneBookEntries.entrySet().stream()
                .filter(a -> a.getValue().equals(phoneNumber))
                .map(Map.Entry::getKey)
                .map(Object::toString)
                .findAny();
    }

    @Override
    public String toString() {
        System.out.println("Hello from PhoneBook's toString method");
        return "PhoneBook{" +
                "phoneBookEntries=" + phoneBookEntries +
                '}';
    }
}
