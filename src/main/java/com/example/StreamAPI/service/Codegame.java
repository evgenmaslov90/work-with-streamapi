package com.example.StreamAPI.service;

import com.example.StreamAPI.entity.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

/*
Exercise for Stream Api
 */
@Service
public abstract class Codegame {

    /*
    Make this code beautiful using Streams and its map operator
     */

    public static Collection<String> mapToUppercase(String... names) {
        Collection<String> uppercaseNames = new ArrayList<>();

        return (Collection<String>) Stream.of(names)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }

    /*
    Now, starting again from a list of names,
    give me the total number of letters in all the names with more than 5 letters
    */
    public static int getTotalNumberOfLettersOfNamesLongerThanFive(String... names) {

        return Stream.of(names)
                .mapToInt(String::length)
                .filter(i -> i > 5)
                .sum();
    }

    /*
    Flatmap
    */
    public static List<String> transform(List<List<String>> collection) {
        List<String> newCollection = new ArrayList<>();
        for (List<String> subCollection : collection) {
            for (String value : subCollection) {
                newCollection.add(value);
            }
        }
        collection.stream().flatMap(Collection::stream).collect(Collectors.toList());
        return newCollection;
    }

    /*
        getOldestPerson
        */
    public static Person getOldestPerson(List<Person> people) {
        Person oldestPerson = new Person("", 0);
        for (Person person : people) {
            if (person.getAge() > oldestPerson.getAge()) {
                oldestPerson = person;
            }
        }
        oldestPerson = people.stream()
                .sorted(Comparator.comparing(Person::getAge).reversed()).findFirst().get();
        return oldestPerson;
    }

    /*
        calculate
        */
    public static int calculate(List<Integer> numbers) {
        int total = 0;
        for (int number : numbers) {
            total += number;
        }
        numbers.stream().reduce(total, (a, b) -> a + b);
        return total;
    }
/*
        Get the names of all kids under the age of 18
*/

    public static Set<String> getKidNames(List<Person> people) {
        Set<String> kids = new HashSet<>();
        for (Person person : people) {
            if (person.getAge() < 18) {
                kids.add(person.getName());
            }
        }

        return people.stream().filter(person -> person.getAge() < 18)
                .map(Person::getName).collect(Collectors.toSet());
    }

    /*
        Partition these people into adults and kids, you'll need a special collector for this one

     */
    public static Map<Boolean, List<Person>> partitionAdults(List<Person> people) {
        Map<Boolean, List<Person>> map = new HashMap<>();
        map.put(true, new ArrayList<>());
        map.put(false, new ArrayList<>());
        for (Person person : people) {
            map.get(person.getAge() >= 18).add(person);
        }

        map = people.stream()
                .collect(Collectors.partitioningBy(person -> person.getAge() >= 18));
        return map;
    }

    /*
       Group these people by nationality, same kind as the previous exercise
     */
    public static Map<String, List<Person>> groupByNationality(List<Person> people) {
        Map<String, List<Person>> map = new HashMap<>();
        for (Person person : people) {
            if (!map.containsKey(person.getNationality())) {
                map.put(person.getNationality(), new ArrayList<>());
            }
            map.get(person.getNationality()).add(person);
        }

        people.stream().collect(Collectors.groupingBy(person -> person.getNationality()));
        return map;
    }
 /*
    Joining

  */
    public static String namesToString(List<Person> people) {
        String label = "Names: ";
        StringBuilder sb = new StringBuilder(label);
        for (Person person : people) {
            if (sb.length() > label.length()) {
                sb.append(", ");
            }
            sb.append(person.getName());
        }
        sb.append(".");
        label = label + people.stream().map(Person::getName).collect(joining(", ")) + ".";
        return sb.toString();
    }

    /*
    Write a method that returns a comma separated string based on a given list of integers.
    Each element should be preceded by the letter 'e' if the number is even,
    and preceded by the letter 'o' if the number is odd. For example,
    if the input list is (3,44), the output should be 'o3,e44'.

  */
    public static String getString(List<Integer> list) {

        return list.stream().map(i ->
        {
            if (i % 2 == 0) {
                return "e" + i;
            } else {
                return "o" + i;
            }
        }).collect(Collectors.joining(","));
    }
}
