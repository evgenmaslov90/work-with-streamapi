package com.example.StreamAPI.service;

import com.example.StreamAPI.entity.Cake;
import com.example.StreamAPI.entity.ChocolateCake;
import com.example.StreamAPI.entity.StuffedChocolateCake;

import java.util.ArrayList;
import java.util.List;

public class UnionUtil {


    public static List<ChocolateCake> union(List<ChocolateCake> firstList, List<ChocolateCake> secondList) {

        List<ChocolateCake> unionList = new ArrayList<>();
        unionList.addAll(firstList);
        unionList.addAll(secondList);
        return unionList;
    }

    public static List<? super ChocolateCake> unionOfChocolateCakesAndStuffedChocolateCakes(List<ChocolateCake> firstList, List<StuffedChocolateCake> secondList){
        List<? super ChocolateCake> unionList = new ArrayList<>();
        unionList.addAll(firstList);
        unionList.addAll(secondList);
        return unionList;
    }
}
