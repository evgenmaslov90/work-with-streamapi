package com.example.StreamAPI.service;

import java.util.stream.IntStream;

public class FizzBuzz {
    public String fizzBuzz(int n) {

        IntStream.rangeClosed(1, n)
                .mapToObj(i -> {
                    if (i % 3 == 0 && i % 5 == 0) {
                        return "FizzBuzz";
                    }
                    if (i % 3 == 0) {
                        return "Fizz";
                    }
                    if (i % 5 == 0) {
                        return "Buzz";
                    }
                    return String.valueOf(i);
                });

        return String.valueOf(n);
    }
}
