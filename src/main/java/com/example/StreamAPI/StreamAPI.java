package com.example.StreamAPI;

import com.example.StreamAPI.entity.ChocolateCake;
import com.example.StreamAPI.entity.StuffedChocolateCake;
import com.example.StreamAPI.service.FizzBuzz;
import com.example.StreamAPI.service.SwapUtil;
import com.example.StreamAPI.service.UnionUtil;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

@SpringBootApplication
public class StreamAPI {

    /*
    public static void UnionUtilmain(String[] args) {
        ChocolateCake choco = new ChocolateCake(1);
        ChocolateCake choco2 = new ChocolateCake(2);

        List<ChocolateCake> chocoList = List.of(choco, choco2);

        ChocolateCake cake = new ChocolateCake(3);
        ChocolateCake cake2 = new ChocolateCake(4);

        List<ChocolateCake> cakeList = List.of(cake, cake2);

        System.out.println(UnionUtil.union(chocoList, cakeList)
                .stream()
                .map(ChocolateCake::getI)
                .map(Objects::toString)
                .collect(joining(", ")));


        StuffedChocolateCake stuffedChocolateCake = new StuffedChocolateCake(1);
        StuffedChocolateCake stuffedChocolateCake2 = new StuffedChocolateCake(2);

        List<StuffedChocolateCake> stuffedChocolateCakes = List.of(stuffedChocolateCake, stuffedChocolateCake2);

        System.out.println(UnionUtil.unionOfChocolateCakesAndStuffedChocolateCakes(chocoList, stuffedChocolateCakes)
                .stream()
                .map(Objects::toString)
                .collect(joining(", ")));

    }
*/
/*    public static void main(String[] args) {

        ChocolateCake choco = new ChocolateCake(1);
        ChocolateCake choco2 = new ChocolateCake(2);
        ChocolateCake choco3 = new ChocolateCake(3);
        ChocolateCake choco4 = new ChocolateCake(4);
        List<ChocolateCake> cakeList = List.of(choco, choco2);

        StuffedChocolateCake stuffedChocolateCake = new StuffedChocolateCake(1);
        StuffedChocolateCake stuffedChocolateCake2 = new StuffedChocolateCake(2);
        System.out.println(Arrays.stream(SwapUtil.swapElements(cakeList.toArray(), 1, 1))
                .map(Objects::toString)
                .collect(joining(", ")));
    }*/

    public static void main(String[] args) {
        FizzBuzz fizzBuzz = new FizzBuzz();

    }

    public static String SearchingChallenge(String str) {
        String[] words = str.split("\\s+");
        String wordWithMostRepeatedLetters = "";
        int maxRepeatedCount = 0;

        for (String word : words) {
            int repeatedCount = getRepeatedLetterCount(word);
            if (repeatedCount > maxRepeatedCount) {
                maxRepeatedCount = repeatedCount;
                wordWithMostRepeatedLetters = word;
            }
        }

        return (maxRepeatedCount > 1) ? wordWithMostRepeatedLetters : "-1";
    }

    public static int getRepeatedLetterCount(String word) {
        int repeatedCount = 0;
        Set<Character> uniqueLetters = new HashSet<>();
        Set<Character> repeatedLetters = new HashSet<>();

        for (char c : word.toCharArray()) {
            if (!uniqueLetters.add(c)) {
                repeatedLetters.add(c);
                repeatedCount = Math.max(repeatedCount, repeatedLetters.size());
            }
        }

        return repeatedCount;
    }
}
