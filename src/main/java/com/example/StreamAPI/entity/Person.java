package com.example.StreamAPI.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    int age;
    String name;
    private String nationality;

    public Person(String s, int i) {
    }
}
