package com.example.StreamAPI.entity;

import java.util.Objects;

public class StuffedChocolateCake extends ChocolateCake{

    int i;

    public StuffedChocolateCake(){
    }

    public StuffedChocolateCake(int i) {
        super();
        this.i = i;
    }

    @Override
    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        StuffedChocolateCake that = (StuffedChocolateCake) o;
        return i == that.i;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), i);
    }
}
