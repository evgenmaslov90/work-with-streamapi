package com.example.StreamAPI.entity;

import java.util.Objects;

public class ChocolateCake extends Cake{

    int i;
    public ChocolateCake(int i) {
        this.i = i;
    }

    public ChocolateCake() {
    }

    public int getI() {
        return i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChocolateCake that = (ChocolateCake) o;
        return i == that.i;
    }

    @Override
    public int hashCode() {
        return Objects.hash(i);
    }
}
